#!/bin/bash

#echo "updating source..."
#git pull

if [ ! -x "$(command -v docker)" ] || [ ! -x "$(command -v docker-compose)" ]; then
  echo 'Docker or docker-compose is not installed yet!'
  sh ./install_docker.sh
else
  echo 'Docker already installed!'
fi

echo "deploying containers..."
docker-compose up -d

echo "updating database..."
docker-compose exec php php artisan migrate

echo "deploying node..."
docker-compose -f docker-compose.yml -f docker-compose.dev.yml run node

echo "all done!"
