#!/bin/bash

if [ ! -x "$(command -v docker)" ] || [ ! -x "$(command -v docker-compose)" ]; then
  echo 'Docker or docker-compose is not installed yet!'
  sh ./install_docker.sh
else
  echo 'Docker already installed!'
fi

echo "instalando composer..."
docker-compose run php composer install

echo "instalando npm..."
docker-compose -f docker-compose.yml -f docker-compose.dev.yml run node npm install

echo "gerando chave do laravel..."
docker-compose run php php artisan key:generate

echo "adicionando permissões para storage"
sudo chmod 777 -R web/storage

echo "all done! Acesse localhost:81"
