#!/bin/bash

if [ ! -x "$(command -v docker)" ] || [ ! -x "$(command -v docker-compose)" ]; then
  echo 'Docker or docker-compose is not installed yet!'
  sh ./install_docker.sh
else
  echo 'Docker already installed!'
fi

echo "deploying containers..."
docker-compose up -d

echo "updating database..."
docker-compose exec php php artisan migrate

echo "all done! Acesse localhost:81"
