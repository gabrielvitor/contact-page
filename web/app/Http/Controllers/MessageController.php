<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * Armazena uma nova mensagem no banco.
     *
     * @param Request $request
     * @return Message
     */
    public function store(Request $request)
    {
        $request->validate([
            'nome' => 'required',
            'email' => 'required',
            'telefone' => 'required',
            'mensagem' => 'required',
            'arquivo' => 'required',
        ]);
        $message = new Message([
            'nome' => $request->input('nome'),
            'email' => $request->input('email'),
            'telefone' => $request->input('telefone'),
            'mensagem' => $request->input('mensagem'),
            'arquivo' => $request->input('arquivo'),
            'ip' => $_SERVER['REMOTE_ADDR'],
            'envio' => date('Y-m-d h:i:s'),
        ]);
        $message->save();

        // armazenar aquivo em disco
        // $request->input('file');
        $this->sendEmail($message->id);
        return $message;
    }

    /**
     * Envia o email com as informações da mensagem passada.
     * @param int $id
     */
    public function sendEmail(int $id)
    {
        $message = Message::find($id);
        // ler arquivo de de configuração
        // enviar email com o conteudo de $message
    }
}
