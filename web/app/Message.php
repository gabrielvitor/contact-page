<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['nome', 'email', 'telefone', 'mensagem', 'arquivo', 'ip', 'envio'];
}
