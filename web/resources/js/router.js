import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// Importa todas as rotas da pasta routes
import Index from './routes/index'
import Error404 from './routes/error404'

export default new VueRouter({
    routes: [
        { path: '/', component: Index },
        { path: '*', component: Error404 }
    ]
})
