window.Vue = require('vue');

// Registra todos os arquivos .vue como componentes válidos
const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Importa o roteador
import router from './router'

// Cria a aplicação
const app = new Vue({
    router
}).$mount('#app');
