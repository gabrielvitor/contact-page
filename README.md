# Contact Page

## Configurar e Instalar Dependências
1. `cp web/.env.example web/.env` para copiar o arquivo `.env.example` da aplicação laravel para `.env`; `cp .env.example .env` para copiar o arquivo `.env` do servidor  

2. (PHP) `docker-compose run php composer install` para instalar o laravel e as dependências do composer.  
*Sem o docker:* `composer install`  

3. (NodeJS - Desenvolvimento) `docker-compose -f docker-compose.yml -f docker-compose.dev.yml run node npm install` para instalar as dependências do npm.  
*Sem o docker:* `node npm install`

4. (Laravel) `docker-compose run php php artisan key:generate` para gerar uma laravel key;
`sudo chmod 777 -R web/storage` para dar permissão à pasta `storage`  

## Executar Projeto
1. sh utils/deploy.sh  

## Estrutura de Pastas
- `web` é o serviço em Laravel + Vue
- `utils` armazena os arquivos de configuração do php e do servidor nginx e os arquivos de execução do projeto.

### Comandos Importante
- (Banco de Dados) Execute `sudo docker-compose exec mysql mysql -u root -p` para ter acesso ao banco de dados. Utilizar senha configurada em `.env`  
*Sem o docker:* `mysql -u root -p`
- (Servidor Web) Execute `docker-compose up -d mysql php nginx` e o servidor será levantado em segundo plano na porta configurada no `.env`  
- (Compilador VueJS - Desenvolvimento) Execute `docker-compose -f docker-compose.yml -f docker-compose.dev.yml run node` e o código do client será monitorado e compilado em tempo real.  
*Sem o docker:* `npm run watch`